﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.DependencyModel;

namespace DependencyInjectionAssistant
{
	public static class AppDomain
	{
		public static List<Assembly> GetAssemblies(string partOfName)
		{
			return DependencyContext
				.Default
				.RuntimeLibraries
				.Where(l => l.Name.Contains(partOfName.ToLower()))
				.Select(l => Assembly.Load(new AssemblyName(l.Name)))
				.ToList();
		}
	}
}