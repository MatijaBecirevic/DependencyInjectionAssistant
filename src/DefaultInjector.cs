﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace DependencyInjectionAssistant
{
	public class DefaultInjector : IInjector
	{
		public void InjectTransient(IServiceCollection services, Type contract, Type implementation)
		{
			services.AddTransient(contract, implementation);
		}

		public void InjectScoped(IServiceCollection services, Type contract, Type implementation)
		{
			services.AddScoped(contract, implementation);
		}

		public void InjectSingleton(IServiceCollection services, Type contract, Type implementation)
		{
			services.AddSingleton(contract, implementation);
		}
	}
}